# What's new

---
## v3.1.1

- Bug fix: Masspec
---

## 3.1.0

- ROSAA simulation modal added.
- Individual page components are subdivided into smaller components for easier mangement and debugging.

- Included Terminal component in Setings for installing python dependencies and debugging.

- Included FELIX plotting in matplotlib to produce quality plots for exporting.

- Event dispatcher added for python process.

- Using Quill editor for report editor.
- Bug fix: Depletion scan dropdown menu auto updated
- Get labview settings from Masspec files
---
